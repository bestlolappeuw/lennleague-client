# README #

### What is this repository for? ###
This is the source code to a program which lets you view your teammates and enemies ranks.

### What should I do for setup? ###

Download the .jar from here https://www.dropbox.com/s/odrx9uownrsbxg4/League.jar?dl=0

currently I do not have a permanent API-Key so you have to use your own RiotGames API-Key. Create a  `_APIKEY.txt`-file in the same directory as the .jar and paste your API-Key inside. 
You can get an API-Key which is valid for 24h at https://developer.riotgames.com

Run the .jar and enter your summoner name (ignore spaces in your summoner name).

Currently only works on EUW since the region is hardcoded, but support for other regions will come soon.

Hopefully I will get a permanent API-Key from RiotGames soon, which makes having your own API-Key irrelevant.