
import javax.swing.*;
import java.awt.*;


public class GUI {
    private static JFrame frame = new JFrame();
    private static JPanel team_1 = new JPanel();
    private static JPanel team_2 = new JPanel();
    private JButton start = new JButton("Start");
    private JButton cache = new JButton("Refresh Cache");
    private static JPanel Jp = new JPanel();
    private JTextField name = new JTextField(15);
    public static JLabel status = new JLabel("<html><font color=\"green\">Type in Summonername</font></html>");
    public static String Key = "";
    GridBagConstraints gbc = new GridBagConstraints();

    public GUI() {
        team_1.setBackground(Color.BLACK);
        team_2.setBackground(Color.BLACK);
        Jp.add(cache);
        Jp.add(name);
        Jp.add(start);
        team_2.add(status);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(500, 150);
        frame.setLayout(new GridBagLayout());
        gbc.gridx = 0;
        gbc.gridy = 0;
        frame.add(team_1, gbc);
        gbc.gridy = 1;
        frame.add(Jp, gbc);
        gbc.gridy = 2;
        frame.add(team_2, gbc);
        frame.setVisible(true);
        frame.setResizable(false);
        Jp.setBackground(Color.BLACK);
        team_1.setBackground(Color.BLACK);
        team_2.setBackground(Color.BLACK);
        frame.getContentPane().setBackground(Color.BLACK);

        team_1.setLayout(new GridLayout(1, 5));
        team_2.setLayout(new GridLayout(1, 5));

        for (int i = 0; i < 10; i++)
            main.sum[i] = new Summoner();

        cache.addActionListener(e -> main.refreshCache());

        start.addActionListener(e -> event());

        name.addActionListener(e -> event());
    }
    public void event(){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {

                for (int i = 0; i < main.sum.length / 2; i++)
                    team_1.remove(main.sum[i].infoSC);

                for (int i = main.sum.length /2; i < main.sum.length; i++)
                    team_2.remove(main.sum[i].infoSC);

                main.start(name.getText(), Key);
            }
        });
        t.start();

    }
    public static void display(){
        team_2.remove(status);
        Jp.add(status);
        for (Summoner s : main.sum)
            s.image();
        frame.setLocationRelativeTo(null);
        int count = 0;
        for (int i = 0; i < main.sum.length; i++)
            if (!main.sum[i].name.equals("?"))
                count++;

        System.out.println(count);

        for (int i = 0; i < count / 2; i++)
            team_1.add(main.sum[i].infoSC);

        for (int i = count / 2; i < count; i++)
            team_2.add(main.sum[i].infoSC);

        for (Summoner s : main.sum) {
            String a = "<HTML><font color=\"white\">";
            a += s.champion + "<br>" + s.name + "<br>" + s.leagues + s.runes + "</font></HTML>";
            s.info.setContentType("text/html");
            s.info.setText(a);
        }
        team_1.setMinimumSize(new Dimension(1400, 300));
        team_1.setPreferredSize(new Dimension(700, 200));
        team_2.setMinimumSize(new Dimension(1400, 300));
        team_2.setPreferredSize(new Dimension(700, 200));
        Jp.setMinimumSize(new Dimension(100, 10));
        Jp.setPreferredSize(new Dimension(700, 40));
        frame.pack();
        frame.setLocationRelativeTo(null);
    }
    public static void status(String text, boolean error){
        if (error)
            status.setText("<html><font color=\"red\">" + text);
        else
            status.setText("<html><font color=\"green\">" + text);
        SwingUtilities.updateComponentTreeUI(frame);

    }
}
