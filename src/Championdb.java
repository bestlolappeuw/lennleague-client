import org.json.JSONObject;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;

public class Championdb {
    public static HashMap<Integer, String> db = new HashMap<>();

    public static void update(){
        try {
            FileReader fr = new FileReader("champions.json");
            int i;
            String aa = "";
            while ((i=fr.read())!=-1)
                aa += (char) i;
            fr.close();

            JSONObject a = new JSONObject(aa);
            JSONObject d = a.getJSONObject("data");
            Iterator<String> it = d.keys();
            JSONObject c;
            while (it.hasNext()) {
                c = d.getJSONObject(it.next());
                db.put(c.getInt("id"), c.getString("key"));
            }
        }catch (Exception e){
            System.out.println(e);
        }
    }
}
