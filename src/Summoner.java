import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;


public class Summoner {
    MyTextPane info = new MyTextPane();

    public JScrollPane infoSC = new JScrollPane(info);
    public String name = "?";
    public String leagues = "";
    public String runes = "";
    public String champion = "?";
    static BufferedImage imgg = null;

    public Summoner() {
        info.setEditable(false);
    }

    public String getchamp() {
        return champion;
    }

    public void setChamp(String ch) {
        champion = ch;
    }

    public static BufferedImage resize(BufferedImage img, int newW, int newH) {
        Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
        BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2d = dimg.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();

        return dimg;
    }

    public void image() {
        try {
            URL url = new URL("http://ddragon.leagueoflegends.com/cdn/img/champion/loading/" + getchamp() + "_0.jpg");
            imgg = ImageIO.read(url);
            imgg = resize(imgg,150,300);
            info.setBG(imgg);

            //imgg = ImageIO.read(new File("images.jpeg"));
        } catch (IOException e) {
        }
    }
}


