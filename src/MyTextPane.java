import javax.swing.*;
import java.awt.*;

public class MyTextPane extends JTextPane{
    Image imgg;
    public MyTextPane() {
        super();
        setOpaque(false);

        // this is needed if using Nimbus L&F - see http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6687960
        setBackground(new Color(0,0,0,0));
    }

    public void setBG(Image imgg) {
        this.imgg = imgg;
        repaint();
    }

    /*@Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(imgg, 0, 0, this);

    }*/
    @Override
    protected void paintComponent(Graphics g) {
        // set background green - but can draw image here too
        //g.setColor(Color.GREEN);
        // g.fillRect(0, 0, getWidth(), getHeight());

        // uncomment the following to draw an image

        g.drawImage(imgg, 0, 0, this);
        super.paintComponent(g);
    }
}
