import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class main {

    public static Summoner[] sum = new Summoner[10];
    static File key = new File("_APIKEY.txt");
    static File champs = new File("champions.json");
    static File runes = new File("runes.json");
    private static String status = "Loading |";

    public static void main(String[] args) {
        try {
        if (!champs.exists()) {
            champs.createNewFile();
            refreshCache();
        }

        if (!runes.exists()) {
            runes.createNewFile();
            refreshCache();
        }
        if (key.exists()) {
            Scanner skey = new Scanner(key);
            GUI.Key = skey.next();
        }
    } catch (Exception ignore) {}
        Championdb.update();
        Runedb.update();
        new GUI();
    }
    public static void start(String name, String key) {
        try {
            name = name.replaceAll("\\s{2,}","");
            JSONObject su = new JSONObject(JSON.HttpResponse("https://euw1.api.riotgames.com/lol/summoner/v3/summoners/by-name/" + name + "?api_key=" + key));
            if (!su.has("status")) {
                long summonerID = su.getLong("id");
                JSONObject jo = new JSONObject(JSON.HttpResponse("https://euw1.api.riotgames.com/lol/spectator/v3/active-games/by-summoner/" + summonerID + "?api_key=" + key));
                if (jo.has("participants")) {
                    JSONArray a = jo.getJSONArray("participants");
                    for (int i = 0; i < a.length(); i++) {
                        sum[i] = new Summoner();
                        sum[i].setChamp(Championdb.db.get(a.getJSONObject(i).getInt("championId")));
                        sum[i].name = a.getJSONObject(i).getString("summonerName");
                        JSONObject l = new JSONObject("{\"leagues\": " + JSON.HttpResponse("https://euw1.api.riotgames.com/lol/league/v3/positions/by-summoner/" + a.getJSONObject(i).getLong("summonerId") + "?api_key=" + key) + "}");
                        JSONArray b = l.getJSONArray("leagues");
                        sum[i].leagues = "<HTML>Leagues:<br>";
                        for (int j = 0; j < b.length(); j++)
                            if (b.getJSONObject(j).toString().contains("rank"))
                                sum[i].leagues += b.getJSONObject(j).getString("tier") + " " + b.getJSONObject(j).getString("rank") + "<br>";
                        JSONArray runes = a.getJSONObject(i).getJSONArray("runes");
                        sum[i].runes = "<HTML>Runes:<br>";
                        for (int j = 0; j < runes.length(); j++) {
                            if (runes.getJSONObject(j).toString().contains("runeId")) {
                                sum[i].runes += runes.getJSONObject(j).getInt("count");
                                sum[i].runes += " times " + Runedb.db.get(runes.getJSONObject(j).getLong("runeId")) + "<br>";
                            }
                        }
                            status = (i + 1) * 10 + "%";

                        GUI.status(status, false);
                        System.out.println(i);
                    }
                    GUI.display();
                    GUI.status("100%", false);
                }
                else {
                    GUI.status("this Summoner is not ingame",true);
                }
            }
            else {
                if (su.getString("status").contains("403"))
                    GUI.status("API Key Expired",true);

                else if (su.getString("status").contains("404"))
                    GUI.status("Summoner not found",true);
                else
                    GUI.status(su.getString("status"),true);
            }
        } catch (JSONException e) {
            System.out.println(e);
        }
    }
    public static void refreshCache(){
        try {
            FileWriter fw = new FileWriter(champs);
            fw.write(JSON.HttpResponse("https://euw1.api.riotgames.com/lol/static-data/v3/champions?locale=en_US&dataById=false&api_key="+GUI.Key));
            fw.flush();
            fw.close();

            fw = new FileWriter(runes);
            fw.write(JSON.HttpResponse("https://euw1.api.riotgames.com/lol/static-data/v3/runes?locale=en_US&api_key="+ GUI.Key));
            fw.flush();
            fw.close();
            Championdb.update();
            Runedb.update();
        } catch (Exception e){
            System.out.print(e);
        }

    }
}
